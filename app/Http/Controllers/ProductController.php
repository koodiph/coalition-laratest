<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use Validator;

class ProductController extends Controller
{

  /**
   * Product model
   * @var Product
   */
  protected $products;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Products $products)
    {
        // $this->middleware('auth');

        $this->products = $products;
    }

    /**
     * product home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return view('product');
    }

    /**
     * List of all product
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $all = $this->products->orderBy('created_at')->get();
        return response()->json($all);
    }

    /**
     * Create new product
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
          'product_name' => 'required|string|max:255',
          'quantity' => 'required|string',
          'price' => 'required|numeric'
        ]);

         if ($validator->fails()) {
             return response()->json($validator->errors()->first(), 422);
         }

        // save to json file
        $product = new Products($inputs);
        $product->total = $product->quantity * $product->price;
        $product->save();

        return response()->json($inputs);
    }

    /**
     * Delete product
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($productId)
    {
        $find = $this->product->find($productId);

        //delete
        $find->destroy();

        return response()->json($find);
    }

    /**
     * Update product
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $productId)
    {
        $inputs = $request->all();

        $find = $this->product->find($productId);
        $find->fill($inputs);
        $find->total = $find->quantity * $find->price;
        $find->save();

        return response()->json($find);
    }

    public function downloadJson(){
  	  $all = $this->products->orderBy('created_at')->get()->toJson();
  	  $file = public_path('upload/json/' . date('YmdHis') . '.json');
  	  \File::put($file, $all);

  	  return response()->download($file);
  	}
}
