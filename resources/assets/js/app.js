
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

var routes = window.Laravel.routes;

updatelist();
function updatelist() {
  console.log(1);
  var $table = $('#product-table tbody');
  $('#product-table tbody').empty();
  $sum = 0;
  $.get(routes.products.list, function(data) {

    for (var i = 0; i < data.length; i++) {
      var pr = data[i];
      var $el = $('<tr />');
      $el.append('<th scope="row">' + pr.id  + '</th>');
      $el.append('<td scope="row">' + pr.product_name  + '</td>');
      $el.append('<td scope="row">' + pr.quantity  + '</td>');
      $el.append('<td scope="row">' + pr.price  + '</td>');
      $el.append('<td scope="row">' + pr.created_at  + '</td>');
      $el.append('<td scope="row">' + pr.total  + '</td>');

      $sum += parseInt(pr.total);
      $table.append($el);
    }

    var $el = $('<tr />');
    $el.append('<th scope="row"></th>');
    $el.append('<th scope="row"></th>');
    $el.append('<th scope="row"></th>');
    $el.append('<th scope="row"></th>');
    $el.append('<th scope="row"><strong>Total:</strong></th>');
    $el.append('<th scope="row">' + $sum + '</th>');
    $table.append($el);
  });
}

$('#product-create-form #save').click(function(e) {
  e.preventDefault();

  var $form = $('#product-create-form');
  var datap = {
    product_name: $form.find('input[name="product_name"]').val(),
    quantity: $form.find('input[name="quantity"]').val(),
    price: $form.find('input[name="price"]').val(),
  };

  $form.find('input').val('');
  $.ajax({
    method: 'POST',
    data: datap,
    url: routes.products.create
  })
  // $.post(routes.products.create, datap)
  .done(function(data) {
    alert('success');
    updatelist();
  })
  .fail(function(errorTxt) {
    alert(errorTxt.responseText);
  });

});
