@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          @include('product/create')
        </div>

        <div class="col-md-12">
            @include('product/list')
        </div>

    </div>
</div>
@endsection
