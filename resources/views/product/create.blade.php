<div class="panel panel-default">
    <div class="panel-heading">New Product</div>

    <div class="panel-body">
      <form class="form-horizontal" id="product-create-form" role="form" method="POST">
          {{ csrf_field() }}

          <div class="form-group{{ $errors->has('product_name') ? ' has-error' : '' }}">
              <label for="product_name" class="col-md-2 control-label">Product name</label>

              <div class="col-md-8">
                  <input id="product_name" type="text" class="form-control" name="product_name" value="{{ old('product_name') }}" required autofocus>

                  @if ($errors->has('product_name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('product_name') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
              <label for="quantity" class="col-md-2 control-label">Quantity in stock</label>

              <div class="col-md-8">
                  <input id="quantity" type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" required autofocus>

                  @if ($errors->has('quantity'))
                      <span class="help-block">
                          <strong>{{ $errors->first('quantity') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
              <label for="price" class="col-md-2 control-label">Price</label>

              <div class="col-md-8">
                  <input id="price" type="number" class="form-control" name="price" value="{{ old('price') }}" required autofocus>

                  @if ($errors->has('price'))
                      <span class="help-block">
                          <strong>{{ $errors->first('price') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="col-md-8">
            <button type="submit" id="save" class="btn btn-primary">
                save
            </button>
          </div>

      </form>
    </div>
</div>
