<div class="panel panel-default" id="product-table">
    <div class="panel-heading">Products</div>
    <div class="panel-body">
      <a href="{{ url('/products/download/json') }}" class="btn btn-primary">Donwload Json</a>
      <br />
      <br />
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product name</th>
                    <th>Quantity in stock</th>
                    <th>Price per item</th>
                    <th>Datetime submitted</th>
                    <th>Total value number</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>
