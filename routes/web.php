<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

Route::get('/', 'ProductController@index');
Route::get('/products/list', 'ProductController@list');
Route::get('/products/download/json', 'ProductController@downloadJson');
Route::post('/products/create', 'ProductController@create');
Route::post('/products/update/{id}', 'ProductController@update');
Route::post('/products/delete/{id}', 'ProductController@delete');
